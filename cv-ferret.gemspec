# -*- encoding: utf-8 -*-
# stub: cv-ferret 0.11.6.19 ruby lib
# stub: ext/extconf.rb

Gem::Specification.new do |s|
  s.name = "cv-ferret"
  s.version = "0.11.6.19"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["ferret"]
  s.autorequire = "ferret"
  s.date = "2016-09-23"
  s.description = "Ferret is a port of the Java Lucene project. It is a powerful indexing and search library."
  s.email = "dbalmain@gmail.com"
  s.executables = ["ferret-browser"]
  s.extensions = ["ext/extconf.rb"]
  s.extra_rdoc_files = ["README", "MIT-LICENSE", "ext/r_index.c", "ext/r_analysis.c", "ext/r_utils.c", "ext/r_search.c", "ext/r_qparser.c", "ext/r_store.c", "ext/ferret.c"]
  s.files = ["MIT-LICENSE", "README", "TUTORIAL", "bin/ferret-browser", "ext/extconf.rb", "ext/ferret.c", "ext/r_analysis.c", "ext/r_index.c", "ext/r_qparser.c", "ext/r_search.c", "ext/r_store.c", "ext/r_utils.c"]
  s.homepage = "http://ferret.davebalmain.com/trac"
  s.rdoc_options = ["--title", "Ferret -- Ruby Indexer", "--main", "README", "--line-numbers", "TUTORIAL"]
  s.rubyforge_project = "ferret"
  s.rubygems_version = "2.4.5.1"
  s.summary = "Ruby indexing library."

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rake>, ["> 0.0.0"])
    else
      s.add_dependency(%q<rake>, ["> 0.0.0"])
    end
  else
    s.add_dependency(%q<rake>, ["> 0.0.0"])
  end
end
